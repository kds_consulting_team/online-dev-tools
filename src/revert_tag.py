import requests
import json


def get_config(link_to_config, sapi_token):
    header = {
        'X-StorageApi-Token': sapi_token
    }
    response = requests.get(link_to_config, headers=header)
    return json.loads(response.text).get("configuration")


def remove_runtime_tag_from_config(config):
    config.pop("runtime", None)
    return config


def push_changes_to_config(link_to_config, sapi_token, config):
    header = {
        'X-StorageApi-Token': sapi_token,
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    payload = {"configuration": json.dumps(config)}
    response = requests.put(link_to_config, headers=header, data=payload)

    return json.loads(response.text).get("configuration")


def revert_tag(url, sapi_token):
    config = get_config(url, sapi_token)
    new_config = remove_runtime_tag_from_config(config)
    push_changes_to_config(url, sapi_token, new_config)
    if new_config == get_config(url, sapi_token):
        return True
    else:
        return False

