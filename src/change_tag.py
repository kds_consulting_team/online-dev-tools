import requests
import json


def get_config(link_to_config, sapi_token):
    header = {
        'X-StorageApi-Token': sapi_token
    }
    response = requests.get(link_to_config, headers=header)
    return json.loads(response.text).get("configuration")


def add_tag_to_config(config, tag_name="test"):
    config["runtime"] = {"image_tag": tag_name}
    return config


def push_changes_to_config(link_to_config, sapi_token, config):
    header = {
        'X-StorageApi-Token': sapi_token,
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    payload = {"configuration": json.dumps(config)}
    response = requests.put(link_to_config, headers=header, data=payload)

    return json.loads(response.text).get("configuration")


def change_tag(url, sapi_token, tag_name):
    config = get_config(url, sapi_token)
    new_config = add_tag_to_config(config, tag_name)
    push_changes_to_config(url, sapi_token, new_config)
    if new_config == get_config(url, sapi_token):
        return True
    else:
        return False

