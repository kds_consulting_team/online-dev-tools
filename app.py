# using python 3
from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from src import change_tag, revert_tag

app = Flask(__name__)
# Flask-WTF requires an enryption key - the string can be anything
app.config['SECRET_KEY'] = 'some?bamboozle#string-foobar'
# Flask-Bootstrap requires this line
Bootstrap(app)
# this turns file-serving to static, using Bootstrap files installed in env
# instead of using a CDN
app.config['BOOTSTRAP_SERVE_LOCAL'] = True


# with Flask-WTF, each web form is represented by a class
# "NameForm" can change; "(FlaskForm)" cannot
# see the route for "/" and "index.html" to see how this is used
class NameForm(FlaskForm):
    name = StringField('Which actor is your favorite?', validators=[DataRequired()])
    submit = SubmitField('Submit')


class ChangeTagForm(FlaskForm):
    config_url = StringField('Link to config that you want to change the tag for', validators=[DataRequired()])
    sapi_token = StringField('Storage API token', validators=[DataRequired()])
    new_tag_name = StringField('Name of tag', validators=[DataRequired()], default="test")
    submit = SubmitField('Submit')


class RevertTagForm(FlaskForm):
    config_url = StringField('Link to config that you want to revert the tag for', validators=[DataRequired()])
    sapi_token = StringField('Storage API token', validators=[DataRequired()])
    submit = SubmitField('Submit')


# define functions to be used by the routes (just one here)

# retrieve all the names from the dataset and put them into a list
def get_names(source):
    names = []
    for row in source:
        name = row["name"]
        names.append(name)
    return sorted(names)


# all Flask routes below

# two decorators using the same function
@app.route('/', methods=['GET', 'POST'])
@app.route('/index.html', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@app.route('/tags/index.html', methods=['GET', 'POST'])
def tag():
    return render_template("tags/index.html")


@app.route('/tags/change_tag.html', methods=['GET', 'POST'])
def change_tag_to_new():
    form = ChangeTagForm()
    message = ""
    if form.validate_on_submit():
        config_url = form.config_url.data
        sapi_token = form.sapi_token.data
        new_tag_name = form.new_tag_name.data
        try:
            changed = change_tag(config_url, sapi_token, new_tag_name)
        except:
            message = "It failed"
            return render_template("tags/change_tag.html", form=form, message=message)
        if changed:
            message = "Tag successfully changed"
        else:
            message = "It failed"
    return render_template("tags/change_tag.html", form=form, message=message)


@app.route('/tags/revert_tag.html', methods=['GET', 'POST'])
def revert_tag_to_default():
    form = RevertTagForm()
    message = ""
    if form.validate_on_submit():
        config_url = form.config_url.data
        sapi_token = form.sapi_token.data
        try:
            reverted = revert_tag(config_url, sapi_token)
        except:
            message = "It failed"
            render_template("tags/revert_tag.html", form=form, message=message)
        if reverted:
            message = "Tag successfully changed"
        else:
            message = "It failed"
    return render_template("tags/revert_tag.html", form=form, message=message)


# keep this as is
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8080, debug=True)
